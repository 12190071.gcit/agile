const mogoose = require('mogoose')
const validator = require ('validator')

const userSchema = new mogoose.Schema({
    name:{
        type :String,
        required: [true, 'Please tell us your name!'],
    },
    email: {
        type:String,
        required:[true,'Please provide your email'],
        unique:true,
        lowercasee:true,
        validate:[validator.isEmail,'please provide a valide email']
    },
    photo:{
        type:String,
        default: 'default.jpg'
    },
    role: {
        type:String,
        enum: ['user','sme','pharmacist','admin'],
        default:'user'
    },
    password:{
        type:String,
        required:[true,'please provide a password'],
        minlength:8,
        slect: false,
    },
    active:{
        type:Boolean,
        default:true,
        select: false,
    },
})
const User =mogoose.model('User',userSchema)
module.exports = User